## Installing Tailwind CSS

1. Create folder name 'Styles'
2. Create 2 files in folder Styles name 'tailwind.css' and 'style.css'
3. Include Tailwind in style.css
   <!-- ./Styles/style.css  -->

   @tailwind base;
   @tailwind components;
   @tailwind utilities;

4. Open Terminal
5. npm init <!--package.json-->
6. npm install tailwindcss <!--package.lock.json-->
7. npx tailwindcss-cli@latest build ./Styles/style.css -o ./Styles/tailwind.css <!-- Css in tailwind.css-->
8. npx tailwindcss init <!--tailwind.config.js-->
9. Customizing configuration into expect:{} in tailwind.config.js
   extend: {
   colors:{
   primarycolor: "#96B47C",
   },

   },
   fontFamily:{
   display: ["Nunito", "sans-serif"],
   }
## install extensions and open project to run on the web page

1. Open Visual Studio Code, then go to extensions.
2. Search for "open in browser".
3. Install it
4. You can run it in two ways:
   * Right click on 'signin.html' file, you will find the option "Open in Browser".
   * Alt + B (windows) or Option + B (macOS)


Updated latest 25/12/2020 .