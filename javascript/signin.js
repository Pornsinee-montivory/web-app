function resetState(elem) {
  const smallElem = elem.parentElement.querySelector("small");
  smallElem.innerText = "";
  elem.classList.remove("invalid");
  form.classList.remove("invalid");
}
function displayError(elem, message) {
  const smallElem = elem.parentElement.querySelector("small");
  smallElem.innerText = message;
  elem.classList.add("invalid");
  form.classList.add("invalid");
}

function validateNull(elem) {
  const val = elem.value;
  // console.log(val);
  if (val == "") {
    const elemName = elem.getAttribute("name");
    displayError(elem, `${elemName} is required`);
    event.preventDefault();
  }

  return true;
}

function validateForm() {
  const usernameElem = document.getElementById("username");
  const passwordElem = document.getElementById("password");

  resetState(usernameElem);
  resetState(passwordElem);

  validateNull(usernameElem);
  validateNull(passwordElem);
}

function run() {
  const formElem = document.querySelector("form");
  formElem.addEventListener("submit", validateForm);
}

run();
