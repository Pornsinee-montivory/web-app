function resetState(elem) {
  const smallElem = elem.parentElement.querySelector("small");
  smallElem.innerText = "";
  elem.classList.remove("invalid");
  form.classList.remove("invalid");
}
function displayError(elem, message) {
  const smallElem = elem.parentElement.querySelector("small");
  smallElem.innerText = message;
  elem.classList.add("invalid");
  form.classList.add("invalid");
}

function validatePsw(elem) {
  const passwordElem = document.getElementById("password");
  const checkpswElem = document.getElementById("checkpsw");
  if (passwordElem.value !== checkpswElem.value) {
    displayError(elem, `Password didn't match `);
    event.preventDefault();
  } else if (passwordElem.value == checkpswElem.value) {
    return true;
  }
}
function validateNull(elem) {
  const val = elem.value;
  if (val == "") {
    const elemName = elem.getAttribute("name");
    displayError(elem, `${elemName} is required`);
    event.preventDefault();
  }
  return true;
}

function validateForm() {
  const firstnameElem = document.getElementById("firstname");
  const lastnameElem = document.getElementById("lastname");
  const usernameElem = document.getElementById("username");
  const passwordElem = document.getElementById("password");
  const checkpswElem = document.getElementById("checkpsw");
  const emailElem = document.getElementById("email");

  resetState(firstnameElem);
  resetState(lastnameElem);
  resetState(usernameElem);
  resetState(passwordElem);
  resetState(checkpswElem);
  resetState(emailElem);

  validateNull(firstnameElem);
  validateNull(lastnameElem);
  validateNull(usernameElem);
  validateNull(passwordElem);
  validateNull(checkpswElem);
  validateNull(emailElem);

  validatePsw(passwordElem);
  validatePsw(checkpswElem);
}

function run() {
  const formElem = document.querySelector("form");
  formElem.addEventListener("submit", validateForm);
}

run();
