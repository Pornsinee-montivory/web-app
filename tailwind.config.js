module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    
    extend: {
      colors:{
        primarycolor: "#96B47C",
      },

    },
    fontFamily:{
      display: ["Nunito", "sans-serif"],
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
